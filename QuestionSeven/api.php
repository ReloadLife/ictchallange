<?php
	/**
	 * @author    MohammadMahdi Afshar <me@reloadlife.me>
	 * @copyright 2018-2019 ReloadLife <me@reloadlife.me>
	 * @license   https://opensource.org/licenses/AGPL-3.0 AGPLv3
	 *
	 */

	require "vendor/autoload.php";

	if (!isset($_REQUEST['e'])) {
		header( "content-type: application/json", true, 400 );
		die( json_encode([
			'message' => 'expression Error',
		]));
	}
	header( "content-type: application/json", true, 200 );
	$math = new \ICTChallenge\Math($_REQUEST['e']);
	echo json_encode([
		'message' => $math->calculate(),
	]);
