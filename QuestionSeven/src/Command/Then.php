<?php
	/**
	 * @author    MohammadMahdi Afshar <me@reloadlife.me>
	 * @copyright 2018-2019 ReloadLife <me@reloadlife.me>
	 * @license   https://opensource.org/licenses/AGPL-3.0 AGPLv3
	 *
	 */
	namespace ICTChallenge\Command;

	class Then {
		protected $matches = [];
		protected $false   = true;

		public function __construct ( array $matches, $false = true ) {
			$this -> matches = $matches;
			$this -> false = $false;
			return $this->false;
		}

		public function __toString () {
			if ($this -> false) {
				return 'true';
			}
			return 'false';
		}

		public function getBool () {
			return $this -> false;
		}

		public function then ( callable $function ) {
			if ($this -> false) {
				return call_user_func_array( $function, $this -> matches );
			}
			return false;
		}
	}