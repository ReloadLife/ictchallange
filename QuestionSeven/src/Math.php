<?php
	/**
	 * @author    MohammadMahdi Afshar <me@reloadlife.me>
	 * @copyright 2018-2019 ReloadLife <me@reloadlife.me>
	 * @license   https://opensource.org/licenses/AGPL-3.0 AGPLv3
	 *
	 */

	namespace ICTChallenge;


	use ChrisKonnertz\StringCalc\Exceptions\StringCalcException;
	use ChrisKonnertz\StringCalc\StringCalc;

	class Math {
		protected $expression;
		protected $result;
		public function __construct ( $expression ) {
			$this->expression = strtolower($expression);
		}


		public function calculate() {
			try {
				$stringCalc = new StringCalc();
				$result = $stringCalc->calculate($this->expression);
				return $result;
			} catch (StringCalcException $exception) {
				 return $exception->getMessage();
			} catch (\Exception $exception) {
				return $exception->getMessage();
			}
		}
	}