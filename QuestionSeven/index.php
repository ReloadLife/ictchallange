<!doctype html>
<html dir='ltr' lang="fa" ng-app="Calculator">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/UiKit/css/uikit.min.css">
    <script src="assets/UiKit/js/uikit.min.js"></script>
    <script src="assets/UiKit/js/uikit-icons.min.js"></script>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
    <script src="assets/angular/angular.min.js"></script>
    <script src="assets/app/app.js"></script>
    <link rel="stylesheet" href="assets/app/app.css">
    <link rel='stylesheet' href='//cdn.rawgit.com/rastikerdar/vazir-font/v19.1.0/dist/font-face.css' />
    <title>Calculator</title>
</head>
<body class='root' id='root' ng-controller="ctrlCalc">
<br><br><br><br><br><br>

<div class='ct'>
    <div class="body project">
        <br>
        <hr class="uk-divider-icon">
        <div class="uk-margin-small uk-container-small uk-align-center" style="text-align: center">
            <button class="uk-button input uk-button-secondary uk-width-1-3 uk-margin-top" ng-click="getAnsw()">
                Calculate
            </button>
            <button class="uk-button input uk-button-secondary uk-width-1-3 uk-margin-top" ng-click="focus(); data = ''">
                Clear
            </button>
            <br><br>
            <input disabled
                   placeholder="معادله ریاضی را با استفاده از دکمه ها وارد کنید..."
                   class="uk-input calculator uk-margin-bottom" id='input_data' ng-model="data"
                   value="{{data}}"
                   style="text-align: center !important; direction: ltr!important;"
            >

            <div class="row">
                <div class="col-6">
                    <div class="row">
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + '9';" class="uk-button input uk-button-secondary uk-width-1">
                                9
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + '8';" class="uk-button input uk-button-secondary uk-width-1">
                                8
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + '7';" class="uk-button input uk-button-secondary uk-width-1">
                                7
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + '6';" class="uk-button input uk-button-secondary uk-width-1">
                                6
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + '5';" class="uk-button input uk-button-secondary uk-width-1">
                                5
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + '4';" class="uk-button input uk-button-secondary uk-width-1">
                                4
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + '3';" class="uk-button input uk-button-secondary uk-width-1">
                                3
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + '2';" class="uk-button input uk-button-secondary uk-width-1">
                                2
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + '1';" class="uk-button input uk-button-secondary uk-width-1">
                                1
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + '.';" class="uk-button input uk-width-1" style="background: #fefefe">
                                .
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + '0';" class="uk-button input uk-button-secondary uk-width-1">
                                0
                            </button>
                        </div>
                        <div class="col-4"></div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + 'pi';" class="uk-button input uk-button-secondary uk-width-1">
                                Pi
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + 'e';" class="uk-button input uk-button-secondary uk-width-1">
                                e
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="row">
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + '+';" class="uk-button input uk-button-danger uk-width-1">
                                +
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + '-';" class="uk-button input uk-button-danger uk-width-1">
                                -
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + '*';" class="uk-button input uk-button-danger uk-width-1">
                                *
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + '/';" class="uk-button input uk-button-danger uk-width-1">
                                /
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + '^';" class="uk-button input uk-button-danger uk-width-1">
                                ^
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + '%';" class="uk-button input uk-button-danger uk-width-1">
                                %
                            </button>
                        </div>
                        <div class="col-4 padding"></div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + '(';" class="uk-button input uk-button-danger uk-width-1">
                                (
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + ')';" class="uk-button input uk-button-danger uk-width-1">
                                )
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + 'sin(';" class="uk-button input uk-button-danger uk-width-1">
                                sin
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + 'cos(';" class="uk-button input uk-button-danger uk-width-1">
                                cos
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + 'tan(';" class="uk-button input uk-button-danger uk-width-1">
                                tan
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + '1 / tan(';" class="uk-button input uk-button-danger uk-width-1">
                                cot
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + 'log(';" class="uk-button input uk-button-danger uk-width-1">
                                log
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + 'sqrt(';" class="uk-button input uk-button-danger uk-width-1">
                                radical
                            </button>
                        </div>
                        <div class="col-4 padding">
                            <button ng-click="focus(); data = data + 'abs(';" class="uk-button input uk-button-danger uk-width-1">
                                ABS ||
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <hr class="uk-divider-icon"><br>
        <h4 class="uk-fieldset">راهنمایی</h4>
        <p  class="uk-fieldset">برای محاسبه لوگاریتم با مبنای غیر 10 میتوانید از تغییر مبنا استفاده کنید.</p>
        <p  class="uk-fieldset">برای محاسبه کناتژانت میتنوانید از 1 تقسیم بر تانژانت زاویه استفاده کنید.</p>
        <br>
        <hr class="uk-divider-icon"><br>
    </div>
</div>
<br><br><br>
<br><br><br>
</body>
</html>