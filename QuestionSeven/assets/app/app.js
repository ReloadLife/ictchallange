let app = angular.module('Calculator', [], function ($httpProvider) {
    app.config(['$qProvider', function ($qProvider) {
        $qProvider.errorOnUnhandledRejections(false);
    }]);
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httpProvider.useApplyAsync(true);
    $httpProvider.defaults.transformRequest = [function (data) {
        /*** The workhorse; converts an object to x-www-form-urlencoded serialization.
         * @param {Object} obj
         * @return {String}
         */
        const param = function (obj) {
            let query = '';
            let name, value, fullSubName, subValue, innerObj, i;
            for (name in obj) {
                value = obj[name];
                if (value instanceof Array) {
                    for (i = 0; i < value.length; ++i) {
                        subValue = value[i];
                        fullSubName = name + '[' + i + ']';
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + '&';
                    }
                } else if (value instanceof Object) {
                    for (subName in value) {
                        subValue = value[subName];
                        fullSubName = name + '[' + subName + ']';
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + '&';
                    }
                } else if (value !== undefined && value !== null) {
                    query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
                }
            }
            return query.length ? query.substr(0, query.length - 1) : query;
        };
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];
});

app.controller('ctrlCalc', function ($scope, $http) {
    $scope.focus = function() {
        document.getElementById('input_data').focus();
    };
    $scope.w8 = false;
    $scope.focus();
    $scope.getAnsw = function() {
        $http({
            method: "POST",
            url: "api.php",
            data: {e: $scope.data}
        }).then(function mySuccess(response) {
            $scope.w8 = true;
            $scope.data = response.data.message;
        }, function myError(response) {
            $scope.data = {};
            alert("Expression error");
        });

    }
});


