<?php
	/**
	 * @author    MohammadMahdi Afshar <me@reloadlife.me>
	 * @copyright 2018-2019 ReloadLife <me@reloadlife.me>
	 * @license   https://opensource.org/licenses/AGPL-3.0 AGPLv3
	 *
	 */
	@session_start();
	ini_set('upload_max_filesize', '1000M');
	ini_set('post_max_size', '1000M');
	require "vendor/autoload.php";
	$url = 'https://185.83.114.129:8086/rest/jet/ict/challenge/answer';
	// header("content-type: application/json");
	// var_dump($_REQUEST, $_FILES);
	if (isset($_REQUEST['password']) && isset($_REQUEST['username']) ) {
		$Client = new \GuzzleHttp\Client(['verify' => false ]);
		if (!isset($_FILES['file'])) {
			$_SESSION['response'] = [
				'message' => "فایل در درخواست نیست"
			];
			return ;
		}
		try {
			$result = $Client -> request( "POST", "https://jetteam.ir:8086/rest/jet/ict/challenge/answer", [
				'multipart' => [
					[
						'name'     => 'username',
						'contents' => $_REQUEST['username'],
					],[
						'name'     => 'password',
						'contents' => $_REQUEST['password'],
					],[
						'name'     => 'challenge_code',
						'contents' => $_REQUEST['challenge_code'],
					],[
						'name'     => 'question_code',
						'contents' => $_REQUEST['question_code'],
					],[
						'name'     => 'team_code',
						'contents' => $_REQUEST['team_code'],
					],[
						'name'     => 'team_name',
						'contents' => $_REQUEST['team_name'],
					],[
						'name'     => 'file',
						'contents' => file_get_contents($_FILES['file']['tmp_name']),
						'filename' => $_FILES['file']['name'],
					],
				]
			]);
			$object = json_decode($result->getBody());
			$_SESSION['response'] = [
				'message_text' => $object->message,
				'data' => $object
			];
		} catch (\GuzzleHttp\Exception\GuzzleException $exception) {
			echo "$exception";
			$_SESSION['response'] = [
				'message_text' => "خطا: " . "{$exception->getMessage()}"
			];
		};
	} else {
		$_SESSION['response'] = [
			'message_text' => 'همه ورودی ها مورد نیاز است'
		];
	}

	include "index.php";