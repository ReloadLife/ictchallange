<!DOCTYPE html>
<html dir='rtl' lang="fa" ng-app="myApp">
<head>
    <meta charset="UTF-8">
    <title>Upload Your Project</title>
    <link rel="stylesheet" href="assets/UiKit/css/uikit-rtl.min.css">
    <script src="assets/UiKit/js/uikit.min.js"></script>
    <script src="assets/UiKit/js/uikit-icons.min.js"></script>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
    <script src="assets/angular/angular.min.js"></script>
    <script src="assets/app/app.js"></script>
    <link rel="stylesheet" href="assets/app/app.css">
    <link rel='stylesheet' href='//cdn.rawgit.com/rastikerdar/vazir-font/v19.1.0/dist/font-face.css' />
</head>
<body class='root' id='root'>
    <br><br><br>
    <div class='ct'>
        <div class="body project" id="UploadProject" ng-controller="projectUploader">
            <br>
            <hr class="uk-divider-icon">
            <form action="api.php" method="POST" enctype="multipart/form-data">
                <fieldset class="uk-fieldset">

                    <legend class="uk-legend uk-margin-right">آپلود پروژه</legend>

                    <div class="uk-margin">
                        <label for="QUsername" class="uk-label">نام کاربری</label>
                        <input class="uk-input input" type="text" ng-model="formData.username" name='username' id="QUsername" placeholder="نام کاربری">
                    </div>

                    <div class="uk-margin">
                        <label for="QPassword" class="uk-label">پسورد</label>
                        <input class="uk-input input" type="password" ng-model="formData.password" name='password' id="QPassword" placeholder="رمز عبور">
                    </div>

                    <div class="uk-margin">
                        <label for="QTeamCode" class="uk-label">کد تیم</label>
                        <input class="uk-input input" type="text" ng-model="formData.teamcode" name='team_code' id="QTeamCode" placeholder="کد تیم">
                    </div>

                    <div class="uk-margin">
                        <label for="QTeamName" class="uk-label">نام تیم</label>
                        <input class="uk-input input" type="text" ng-model="formData.TeamName" name='team_name' id="QTeamName" placeholder="نام تیم">
                    </div>

                    <div class="uk-margin">
                        <label for="QuestionID" class="uk-label">کد سوال</label>
                        <input class="uk-input input" type="text" ng-model="formData.QuestionID" name='question_code' id="QuestionID" placeholder="کد سوال">
                    </div>


                    <div class="uk-margin">
                        <label for="c" class="uk-label">کد چالش</label>
                        <select class="uk-select input" id="c" name="challenge_code" ng-model="formData.Lanauge">
                            <option value="php">PHP</option>
                            <option value="php">Java</option>
                            <option value="net">.NET</option>
                            <option value="python">Python</option>
                            <option value="image">Image</option>
                        </select>
                    </div>

                    <div class="uk-margin">
                        <label for="file" class="uk-label">فایل zip چالش</label>
                        <div class="input p-0" >
                            <input type="file" id="file" name="file" class="uk-input uk-button uk-background-default uk-button-default input"
                                   ng-model="formData.file" ng-files="console.log($files)" select-ng-files>
                        </div>
                    </div>

                    <div class="uk-margin">
                        <button class="uk-button uk-button-danger col-md-12 input" <?php if (isset($_SESSION['response'])) {echo 'disable';}?> >ارسال</button>
                    </div>

                </fieldset>
            </form>
            <br>
            <div id="result">
                <pre><code><?php if (isset($_SESSION['response'])) {
                    echo "file Sha: " . $_SESSION['response']['data']->data->file_sha256;
                }?></code></pre>
                <script>
                    <?php if (isset($_SESSION['response'])) {
                       echo "alert('{$_SESSION['response']['message_text']}')";
                       unset($_SESSION['response']);
                    }?>
                </script>
            </div>
            <hr class="uk-divider-icon"><br>
        </div>
        <br><br><br>
        <div class="body score" id="ScoreGet" ng-controller="ScoreGet">
            <br><hr class="uk-divider-icon">
            <form>
                <fieldset class="uk-fieldset">

                    <legend class="uk-legend uk-margin-right">امتیاز</legend>

                    <div class="uk-margin">
                        <label for="2QUsername" class="uk-label">نام کاربری</label>
                        <input class="uk-input input" type="text" ng-model="formData.username" id="2QUsername" placeholder="Username">
                    </div>

                    <div class="uk-margin">
                        <label for="2QPassword" class="uk-label">پسورد</label>
                        <input class="uk-input input" type="password" ng-model="formData.password" id="2QPassword" placeholder="Password">
                    </div>


                    <div class="uk-margin">
                        <button class="uk-button uk-button-danger col-md-12 input" ng-click="getScore()">دریافت امتیاز</button>
                    </div>

                </fieldset>
            </form>
            <br>
            <div id="result2" lang="json">
                <table ng-if="responsee" class="uk-table uk-table-striped uk-table-hover uk-table-divider">
                    <caption></caption>
                    <thead>
                        <tr>
                            <th class="table-active" style="text-align: left;background: #fff;">Score</th>
                            <th class="table-active" style="text-align: left;background: #fff;">Team Code</th>
                        </tr>
                    </thead>
                    <tbody ng-repeat="iRes in dataResponse.data.answer_info">
                        <tr>
                            <td>{{iRes.score}}</td>
                            <td>{{iRes.team_code}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <hr class="uk-divider-icon"><br>
        </div>
        <br><br>
    </div>
</body>
</html>