<?php
	/**
	 * @author    MohammadMahdi Afshar <me@reloadlife.me>
	 * @copyright 2018-2019 ReloadLife <me@reloadlife.me>
	 * @license   https://opensource.org/licenses/AGPL-3.0 AGPLv3
	 *
	 */
	@session_start();
	require "vendor/autoload.php";
	$url = 'https://185.83.114.129:8086/rest/jet/ict/team/score';
	header("content-type: application/json");
	if (isset($_REQUEST['password']) && isset($_REQUEST['username'])) {
		$Client = new \GuzzleHttp\Client(['verify' => false ]);
		try {
			$result = $Client -> request( "POST", "https://185.83.114.129:8086/rest/jet/ict/team/score", [
				'multipart' => [
					[
						'name'     => 'username',
						'contents' => $_REQUEST['username'],
					],[
						'name'     => 'password',
						'contents' => $_REQUEST['password'],
					],[
						'name'     => 'challenge_code',
						'contents' => "Php",
					],[
						'name'     => 'team_code',
						'contents' => "165680",
					],
				]
			]);
			echo $result->getBody();
		} catch (\GuzzleHttp\Exception\GuzzleException $exception) {
			echo json_encode([
				'message_text' => "یوزرنیم و پسورد اشتباه است"
			]);
		};
	} else {
		echo json_encode([
			'message_text' => "یوزرنیم و پسورد باید به درستی وارد شود"
		]);
	}