let app = angular.module('myApp', [], function ($httpProvider) {
    app.config(['$qProvider', function ($qProvider) {
        $qProvider.errorOnUnhandledRejections(false);
    }]);
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httpProvider.useApplyAsync(true);
    $httpProvider.defaults.transformRequest = [function (data) {
        /*** The workhorse; converts an object to x-www-form-urlencoded serialization.
         * @param {Object} obj
         * @return {String}
         */
        const param = function (obj) {
            let query = '';
            let name, value, fullSubName, subValue, innerObj, i;
            for (name in obj) {
                value = obj[name];
                if (value instanceof Array) {
                    for (i = 0; i < value.length; ++i) {
                        subValue = value[i];
                        fullSubName = name + '[' + i + ']';
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + '&';
                    }
                } else if (value instanceof Object) {
                    for (subName in value) {
                        subValue = value[subName];
                        fullSubName = name + '[' + subName + ']';
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + '&';
                    }
                } else if (value !== undefined && value !== null) {
                    query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
                }
            }
            return query.length ? query.substr(0, query.length - 1) : query;
        };
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];
});

app.config(['$compileProvider', function ($compileProvider) {$compileProvider.aHrefSanitizationWhitelist(/^\s*(tg|skype|javascript|ftp|mailto|file|sms|tel|http|https?|js|telegram):/);}]);

app.directive("selectNgFiles", function() {
    return {
        require: "ngModel",
        link: function postLink(scope,elem,attrs,ngModel) {
            elem.on("change", function(e) {
                var files = elem[0].files;
                ngModel.$setViewValue(files);
            })
        }
    }
});
app.directive('ngFiles', ['$parse', function ($parse) {
    function fn_link(scope, element, attrs) {
        var onChange = $parse(attrs.ngFiles);
        element.on('change', function (event) {
            onChange(scope, { $files: event.target.files });
        });
    }
    return {
        link: fn_link
    }
} ]);

app.controller('projectUploader', function ($scope, $http) {
    $scope.formData = {
        file: {},
        password: '',
        teamcode: '',
        TeamName: '',
        QuestionID: '',
        Lanauge: '',
    };
    $scope.dataResponse = "";
    $scope.true = true;
    $scope.sendit = function () {
        if ($scope.formData.file.length < 1 ) {
            alert("فایلی انتخاب نکردید !");
            return false;
        }
        var formdata = new FormData();
        angular.forEach($scope.formData.file, function (value, key) {
            formdata.append(key, value);
        });

        return $http({
            method: "POST",
            url: "api.php",
            data: {
                username: $scope.formData.username,
                password: $scope.formData.password,
                team_name: $scope.formData.teamcode,
                team_code: $scope.formData.TeamName,
                question_code: $scope.formData.QuestionID,
                challenge_code: $scope.formData.Lanauge,
                file: $scope.formData.file[0],
            }
        }).then(function (response) {
            $scope.dataResponse = response.data;
            if (response.data.message) {
                alert(response.data.message);
                $scope.true = false;
            }
        } );
    };
});

app.controller('ScoreGet', function ($scope, $http) {
    $scope.formData = {};
    $scope.dataResponse = {};
    $scope.true = true;
    $scope.responsee = false;
    $scope.getScore = function () {
        return $http({
            method: "POST",
            url: "api-2.php",
            data: {
                username: $scope.formData.username,
                password: $scope.formData.password,
            }
        }).then(function (response) {
            $scope.dataResponse = response.data;
            if (response.data.message_text) {
                alert(response.data.message_text);
                $scope.true = false;
                response.data.answer_info = [];
                $scope.responsee = false;
            } else {
                $scope.responsee = true;
            }
        } );
    };
});

