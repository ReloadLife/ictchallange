<?php
	/**
	 * @author    MohammadMahdi Afshar <me@reloadlife.me>
	 * @copyright 2018-2019 ReloadLife <me@reloadlife.me>
	 * @license   https://opensource.org/licenses/AGPL-3.0 AGPLv3
	 *
	 */

	namespace ICTChallenge;

	use GuzzleHttp\Client;
	use ICTChallenge\Command\Command;
	use PHPHtmlParser\Dom;
	use Psr\Http\Message\RequestInterface;
	use Psr\Http\Message\ResponseInterface;
	use Psr\Http\Message\UriInterface;

	try {
		require "vendor/autoload.php";
		header( "content-type: application/json" );
		if (!isset( $_REQUEST['url'] )) {
			die ( json_encode( [
				'ok' => false, 'message' => 'no `url` in request',
			] ) );
		}
		$url = $_REQUEST['url'];
		$url = new Command( $url );
		if (!( $url -> startsWith( 'http://' ) || $url -> startsWith( 'https://' ) )) {
			die ( json_encode( [
				'ok' => false, 'message' => '`url` is not valied',
			] ) );
		}
		$client = new Client( [
			'headers' => [
				'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
			], 'timeout' => 5.0, 'verify' => false,
		] );
		$URI = $url->toString();
		try {
			$result = $client -> request( "GET", $url -> toString(), [ 'allow_redirects' => [
				'max'             => 10,
				'strict'          => true,
				'referer'         => true,
				'protocols'       => ['https', 'http'],
				'on_redirect'     => function(
					RequestInterface $request,
					ResponseInterface $response,
					UriInterface $uri
				) use( &$URI ) {
					$URI = $uri;
				},
				'track_redirects' => true
			] ]);
			$resultBody = new Command( $result -> getBody() );
			$Desc = false;
			if ($resultBody -> regex( ".*<title.*>{title}</title>.*", $regex )) {
				if ($resultBody -> regex( ".*content='{description}'.*property=.*description.*", $DEsc ) || $resultBody -> regex( ".*content=\"{description}\".*property=.*description.*", $DEsc )) {
					$Desc = $DEsc['description'];
				} elseif ($resultBody -> regex( ".*property=.*description.*content=\"{description}\".*", $DEsc ) || $resultBody -> regex( ".*property=.*description.*content='{description}'.*", $DEsc )) {
					$Desc = $DEsc['description'];
				} else {
					$dom = new Dom();
					$dom -> load( $resultBody -> toString() );
					foreach ($dom -> find( 'meta' ) as $child) {
						if (( $child -> name == 'description' ) || $child -> property == 'description') {
							$Desc = $child -> content;
						}
					}
					if (!isset( $Desc )) {
						if (isset( $dom -> find( 'p' )[0] )) {
							$Desc = $dom -> find( 'p' )[0] -> text;
						} elseif (isset( $dom -> find( 'h1' )[0] )) {
							$Desc = $dom -> find( 'h1' )[0] -> text;
						} elseif (isset( $dom -> find( 'h2' )[0] )) {
							$Desc = $dom -> find( 'h2' )[0] -> text;
						} elseif (isset( $dom -> find( 'h3' )[0] )) {
							$Desc = $dom -> find( 'h3' )[0] -> text;
						} elseif (isset( $dom -> find( 'h4' )[0] )) {
							$Desc = $dom -> find( 'h4' )[0] -> text;
						}
					}
				}

				if ($resultBody -> regex( ".*content='{image}'.*property=.*image.*", $ImageRegex ) || $resultBody -> regex( ".*content=\"{image}\".*property=.*image.*", $ImageRegex )) {
					$Image = $ImageRegex['image'];
				} elseif ($resultBody -> regex( ".*property=.*image.*content=\"{image}\".*", $ImageRegex ) || $resultBody -> regex( ".*property=.*image.*content='{image}'.*", $ImageRegex )) {
					$Image = $ImageRegex['image'];
				} else {
					$dom = new Dom();
					$dom -> load( $resultBody -> toString() );
					foreach ($dom -> find( 'meta' ) as $child) {
						if (( $child -> name == 'image' ) || ( $child -> itemprop == 'image' ) || $child -> property == 'image') {
							$Image = $child -> content;
						} elseif (strstr( $child -> name, 'image' ) || strstr( $child -> itemprop, 'image' ) || strstr( $child -> property, 'image' )) {
							$Image = $child -> content;
						}
					}
					if (!isset( $Image )) {
						$Image = false;
					}
				}
				$image = new Command( $Image );
				if ($image -> startsWith( '//' )) {
					$Image = 'https:' . $image -> toString();
				} elseif ($image -> startsWith( '/' )) {
					$Image = $URI . '' . $image -> toString();
				}

				if ($resultBody -> regex( ".*content='{video}'.*property=.*video.*", $VideoRe ) || $resultBody -> regex( ".*content=\"{video}\".*property=.*video.*", $VideoRe )) {
					$VideoUrl = $VideoRe['video'];
				} elseif ($resultBody -> regex( ".*property=.*video.*content=\"{video}\".*", $VideoRe ) || $resultBody -> regex( ".*property=.*video.*content='{video}'.*", $VideoRe )) {
					$VideoUrl = $VideoRe['video'];
				} else {
					$VideoUrl = false;
					if ($url->find('youtube.com')) {
						$url->regex('watch?v={vid}', $V);
						$VideoUrl = "https://www.youtube.com/embed/{$V['vid']}";
					}
				}
				die ( json_encode( [
					'ok' => true, 'message' => [
						'url' => $url->toString(), 'title' => html_entity_decode( $regex['title'] ),
						'description' => $Desc, 'image' => $Image, 'video_stream' => $VideoUrl,
					],
				] ) );
			} else {
				die ( json_encode( [
					'ok' => false, 'message' => "not a html website",
				] ) );
			}

		} catch ( \GuzzleHttp\Exception\GuzzleException $exception ) {
			if ($exception -> getCode() === 0) {
				die ( json_encode( [
					'ok' => false, 'code' => $exception -> getCode(),
					'message' => [ 'error_message' => "i can not access to site due to restrictions", ],
				] ) );
			}
			die ( json_encode( [
				'ok' => false, 'code' => $exception -> getCode(), 'message' => [
					'error_message' => "maybe i can not access to site due to restrictions",
					'exception' => $exception -> getMessage(),
				],
			] ) );
		}
	} catch ( \Throwable $e ) {
		die ( json_encode( [
			'ok' => false, 'code' => $e -> getCode(), 'message' => $e -> getMessage(),
		] ) );
	}


