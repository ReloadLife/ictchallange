<?php
	/**
	 * @author    MohammadMahdi Afshar <me@reloadlife.me>
	 * @copyright 2018-2019 ReloadLife <me@reloadlife.me>
	 * @license   https://opensource.org/licenses/AGPL-3.0 AGPLv3
	 *
	 */
	namespace ICTChallenge\Command;

	class Command {

		protected $text;
		protected $matches;

		public function __construct ( $text ) {
			$this -> text = $text;
		}

		public function preg_match ( $pattern ) {
			return preg_match( $pattern, $this->text, $this->matches);
		}

		public function _getMatches () {
			return array_slice($this -> matches, 1);
		}

		public function getMatches () {
			return array_unique(array_slice($this -> matches, 1));
		}

		public function in ( $needle ) {
			if (!is_array( $needle )) {
				$needle = [ $needle ];
			}
			foreach ($needle as $need) {
				if ( strstr( $this->text, $need ) ) return true;
			}
			return false;
		}

		public function sub ( $start, $length = null ) {
			return new Command( substr( $this -> text, $start, $length ) );
		}

		public function endsWith ( $needle ) {
			if (!is_array( $needle )) {
				$needle = [ $needle ];
			}
			foreach ($needle as $need) {
				$length = strlen( $need );
				if ($length == 0) {
					continue;
				}
				if (substr( $this -> text, -$length ) === $need) {
					return true;
				}
			}
			return false;
		}

		public function startsWith ( $needle ) {
			if (!is_array( $needle )) {
				$needle = [ $needle ];
			}
			foreach ($needle as $need) {
				$length = strlen( $need );
				if (( substr( $this -> text, 0, $length ) === $need )) {
					return true;
				}
			}
			return false;
		}

		public function toString () {
			return (string)$this -> text;
		}

		public function __toString () {
			return $this->toString();
		}

		public function equals ( $needle ) {
			return $this -> text == $needle;
		}

		public function find ( $needle ) {
			return strstr($this -> text, $needle);
		}

		public function equals_really ( $needle ) {
			return $this -> text === $needle;
		}

		public function regex ( $patterns, & $args = false ) {
			if (!is_array($patterns)) {
				$patterns = [$patterns];
			}

			foreach ($patterns as $pattern) {
				$pattern = str_replace( '/', '\/', $pattern );
				if (is_array( $pattern )) $pattern = $pattern[0];
				$text = '/^' . preg_replace( Regex::$NameRegex, '(?<$1>.*)', $pattern ) . ' ?$/miu';
				if ( (bool)$this->preg_match($text) ) {
					$args = $this->getMatches();
					return true;
				}
			}
			return false;
		}

		public function matcher ( $patterns ) {
			if (!is_array($patterns)) {
				$patterns = [$patterns];
			}

			foreach ($patterns as $pattern) {
				$pattern = str_replace( '/', '\/', $pattern );
				if (is_array( $pattern )) $pattern = $pattern[0];
				$text = '/^' . preg_replace( Regex::$NameRegex, '(?<$1>.*)', $pattern ) . ' ?$/miu';
				if ( (bool)$this->preg_match($text) ) {
					return new Then($this->getMatches(), true);
				}
			}
			return new Then([], false);
		}

		public function toInt () {
			return (int)intval($this -> text);
		}

		public function inArray ( array $array ) :bool {
			return in_array($this->text, $array);
		}

		public function isInt () {
			return is_numeric( $this -> text );
		}

	}
