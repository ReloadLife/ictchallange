<?php
	/**
	 * @author    MohammadMahdi Afshar <me@reloadlife.me>
	 * @copyright 2018-2019 ReloadLife <me@reloadlife.me>
	 * @license   https://opensource.org/licenses/AGPL-3.0 AGPLv3
	 *
	 */
	namespace ICTChallenge\Command;

	class Regex {

		public static $NameRegex = '/\{((?:(?!\d+,?\d+?)\w)+?)\}/';

	}
