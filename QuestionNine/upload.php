<?php

	header( "Content-Type:application/json" );
	require "vendor/autoload.php";
	try {
		class cache extends \Redis {
			protected $redis;

			public function __construct () {
				parent ::__construct();
				$this -> connect( 'localhost' );
				$this -> setOption( \Redis::OPT_PREFIX, 'uploader' );
			}
		}

		class Upload {
			protected $file = '';

			public function __construct ( $filename ) {
				$this -> file = ( $filename ) . '.tmp';
			}

			public function write ( $input ) {
				file_put_contents( $this -> file, $input, FILE_APPEND );
			}

			public function decode ( $newFileName ) {
				file_put_contents( $newFileName, base64_decode( file_get_contents( $this -> file ) ) );
			}

			public function getUrl ($newFileName) {
				return $newFileName;
			}
		}

		class requestCatcher {
			protected $method;

			public function __construct () {
				$this -> method = $_SERVER['REQUEST_METHOD'];
				$this -> path = $_SERVER['PATH_INFO'];
				if (empty( $_SERVER['PATH_INFO'] )) {
					$this -> path = $_SERVER['REQUEST_URI'];
				}
				$this -> path = new ICTChallenge\Command\Command( $this -> path );
			}

			public function isPut () {
				return strtoupper( $this -> method ) == 'PUT';
			}

			public function isPost () {
				return strtoupper( $this -> method ) == 'POST';
			}

			public function isGet () {
				return strtoupper( $this -> method ) == 'GET';
			}

			public function getKey () {
				if ($this -> path -> regex( '/key:{key}', $regex )) {
					return $regex['key'];
				}
				return false;
			}

			public function _GET () {
				return $_GET;
			}

			public function _POST () {
				return $_POST;
			}

			public function _PUT () {
				return file_get_contents( 'php://input' );
			}

		}
		$requestCatcher = new requestCatcher();
		$redis = new cache();
		if ($requestCatcher -> isGet()) {
			$fileName = $requestCatcher -> _GET()['file_name'];
			if ($redis -> hExists( 'upload:tokens', $fileName, )) {
				$token = $redis -> hGet( 'upload:tokens', $fileName, );

				die ( json_encode( [
					'ok' => true, 'key' => $token, 'exist' => true,
					'uploaded_slice' => $redis -> hGet( 'upload:' . $token, 'uploaded_slice', )
				] ) );
			} else {
				$token = uniqid();
				$redis -> hSet( 'upload:tokens', $fileName, $token );
				$redis -> hSet( 'upload:' . $token, 'file_name', $fileName );
				$redis -> hSet( 'upload:' . $token, 'uploaded_slice', 0 );
				$redis -> hSet( 'upload:' . $token, 'tmp_name', $token . '.tmp' );
				$redis -> hSet( 'upload:' . $token, 'expire', ( time() + ( 60 * 60 * 24 * 3 ) ) );
				die ( json_encode( [
					'ok' => true, 'key' => $token,
				] ) );
			}
		} elseif ($requestCatcher -> isPut()) {
			$upload = new Upload( $key = $requestCatcher -> getKey() );
			$upload -> write( $requestCatcher -> _PUT() );
			$timeDone = $redis -> hGet( 'upload:' . $key, 'uploaded_slice', );
			if (!is_numeric( $timeDone )) {
				$timeDone = 0;
			}
			$redis -> hSet( 'upload:' . $key, 'uploaded_slice', $timeDone + 1 );
			die ( json_encode( [
				'ok' => true, 'key' => $key,
			] ) );
		} elseif ($requestCatcher -> isPost()) {
			$upload = new Upload( $key = $requestCatcher -> getKey() );
			$FileName = $redis -> hGet( 'upload:' . $key, 'file_name', );
			$upload -> decode( $FileName );
			die ( json_encode( [
				'ok' => true, 'key' => $key, 'url' => '/' . $upload -> getUrl( "http://31.184.131.71/QuestionNine" . $FileName )
			] ) );
		} else {
			var_dump($_SERVER, $_REQUEST, );
		}
	} catch ( Throwable $exception ) {
		die ( json_encode( [
			'ok' => false, 'message' => $exception->getMessage()
		] ) );
	}


