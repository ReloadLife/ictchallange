import React from 'react';


class App extends React.Component{
    contents = null;
    constructor(props){
        super(props);
        var reader = new FileReader();
        this._fileHandler = this._fileHandler.bind(this);
        
    }
    _fileHandler(e){
        var base64 = require('js-base64').Base64;
        console.log(e.target);
        var file = e.target.files[0];
        console.log(file)
        if (!file) {
            return;
        }
        
        console.log(reader.filename)
        reader.onload = function(e) {
            var contents = e.target.result;
            contents = base64.encode(contents);
            console.log(contents);
            var lent = contents.length / 100;
            console.log(lent)
            for(var i = 0; i < lent;i++){
                console.log(contents.slice(i*100,i*100+100))
            }
        };
        
        reader.readAsText(file);
    }
    render(){
        return(
            <>
                <input type="file" id="file-input" onChange={this._fileHandler} />
            </>
        )
    }
}

export default App;